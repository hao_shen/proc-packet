package utils

import (
	"github.com/google/gopacket/pcap"
	"github.com/shirou/gopsutil/net"
	"log"
)

// GetAllDevices 获取所有设置
func GetAllDevices() (res []pcap.Interface) {
	devices, err := pcap.FindAllDevs()
	if err != nil {
		log.Fatal(err)
		return
	}
	log.Println("len -> ", len(devices))
	for _, device := range devices {
		//log.Printf("%+v\n", device)
		res = append(res, device)
	}
	return
}

// GetNeedConnStats 获取pid的指定连接
func GetNeedConnStats(pid int32) (res []net.ConnectionStat) {
	//for _, p := range GetProcFrontByName(name) {
	cons, err := net.ConnectionsPid("tcp", pid)
	if err != nil {
		log.Println(err.Error())
		return
	}
	res = append(res, cons...)
	//}
	return
}
