package utils

import (
	"bytes"
	"log"
	"os/exec"
)

func ExecCmd(call string) (res, e string) {
	cmd := exec.Command("powershell", "-Command", call)
	var stderr bytes.Buffer
	cmd.Stderr = &stderr
	output, err := cmd.Output()
	if err != nil {
		log.Printf("call ExecCmd failed, %s -> %s", cmd.String(), stderr.String())
		return "", stderr.String()
	}
	return string(output), ""
}

func SliceIndex[T int | string](arr []T, item T) int {
	for i, v := range arr {
		if v == item {
			return i
		}
	}
	return -1
}
