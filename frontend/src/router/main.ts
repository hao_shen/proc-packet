import {createRouter, createWebHashHistory} from 'vue-router'

import HomeView from '../components/HelloWorld.vue'

const router = createRouter({
    history: createWebHashHistory(),
    routes: [{
        path: '/',
        name: 'home',
        component: HomeView
    }]
})

export default router