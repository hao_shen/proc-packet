# ProcPacket

#### 介绍
一个简洁的抓包软件，只需输入进程名即可获取该进程所有的包，支持显示对话，目前win支持，mac和linux在going。。。

#### 软件架构
wails+go,前端使用vue，后端纯go实现


#### 安装教程

1. https://wails.io/，wails官网文档进行配置
2. wails dev命令即可运行

#### 使用说明

1.  输入进程名即可进行搜索
2.  单击显示包详情，双击显示包参与的对话
3.  支持设备选择，进程id选择

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
