package model

type Proc struct {
	Pid  int32
	Name string
	Line string
}
