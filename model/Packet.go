package model

type Packet struct {
	Index             int
	SrcIp             string
	SrcPort           string
	DstIp             string
	DstPort           string
	Protocol          string
	Window            uint16
	Data              string
	DataString        string
	Pid               int32
	DeviceName        string
	DeviceDescription string
	Method            string
}
