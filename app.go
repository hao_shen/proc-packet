package main

import (
	"context"
	"fmt"
	"github.com/google/gopacket/pcap"
	"procPacket/model"
	"procPacket/services"
	"procPacket/utils"
)

// App struct
type App struct {
	ctx context.Context
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
	services.GetPacket(ctx)
}

// Greet returns a greeting for the given name
func (a *App) Greet(name string) string {
	return fmt.Sprintf("Hello %s, It's show time!", name)
}

func (a *App) GetProcIdByName(name string) (res []*model.Proc) {
	return utils.GetProcFrontByName(name)
}

func (a *App) SetSelectPid(pids []int32) {
	services.SetSelectPid(pids)
}

func (a *App) SetNeedDevices(devs []string) {
	services.SetNeedDevices(devs)
	services.CancelFunc()
	services.GetPacket(a.ctx)
}

func (a *App) GetAllDevices() (devs []pcap.Interface) {
	return services.AllDevices
}
